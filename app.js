const express = require("express");
const bodyParser = require("body-parser");
const redisClient = require("redis").createClient();

const port = 3000;

const app = express();

redisClient.connect();
redisClient.on("connect", () => console.log("Redis Client Connected"));
redisClient.on("error", (err) => console.log("Redis Client Error", err));

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.post("/user/save", async (req, res) => {
  try {
    var check = await redisClient.get(req.body.id);
    if (check) {
      return res.json({
        error: true,
        message: "Already saved in redis",
      });
    }
    await redisClient.set(
      req.body.id,
      JSON.stringify({
        Name: req.body.name,
        CreatedAt: req.body.created_at,
      })
    );
    return res.json({
      error: 0,
      message: "",
    });
  } catch (error) {
    return res.json({
      error: true,
      message: error.message,
    });
  }
});

app.get("/user/detail/:id", async (req, res) => {
  try {
    var check = await redisClient.get(req.params.id);
    if (!check) {
      return res.json({
        error: true,
        message: "Data not found",
      });
    }
    var json = JSON.parse(check);
    var response = {
      name: json.Name,
      created_at: json.CreatedAt,
    };
    return res.json({
      error: 0,
      message: "",
      data: response,
    });
  } catch (error) {
    return res.json({
      error: true,
      message: error.message,
    });
  }
});

app.use((request, response, next) => {
  const error = new Error("API Invalid");
  next(error);
});

app.use((error, request, response, next) => {
  return response.json({
    error: true,
    message: error.message || "Failed",
  });
});

app.listen(port, () => {
  console.log(`Server started at port: ${port}`);
});
